/************************************************************
*  * HONGHE CONFIDENTIAL
* __________________
* Copyright (C) 2014-2015 HONGHE Technologies. All rights reserved.
*
* NOTICE: All information contained herein is, and remains
* the property of HONGHE Technologies.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from HONGHE Technologies.
* The author is knight(卢远强), ios software engineer.
*/

import UIKit

class CustomCell: UITableViewCell {

    @IBOutlet var nameIcon : UIImageView!
    
    @IBOutlet var nameLabel : UILabel!
    
    @IBOutlet var numbers : UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
