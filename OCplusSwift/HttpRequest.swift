/************************************************************
*  * HONGHE CONFIDENTIAL
* __________________
* Copyright (C) 2014-2015 HONGHE Technologies. All rights reserved.
*
* NOTICE: All information contained herein is, and remains
* the property of HONGHE Technologies.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from HONGHE Technologies.
* The author is knight(卢远强), ios software engineer.
*/

import UIKit

class HttpRequest: NSObject {
    
    class func requestWithURL(urlString:String,completionHandle:(data:AnyObject)->Void){
        
        var url = NSURL.URLWithString(urlString)
        var req = NSURLRequest(URL:url)
        var queue = NSOperationQueue()
        NSURLConnection.sendAsynchronousRequest(req,queue:queue,completionHandler:{response,data,error in
            if (error != nil)
            {
                dispatch_async(dispatch_get_main_queue(),
                    {
                        println(error)
                        completionHandle(data:NSNull())
                    })
            }
            else{
                
                let jsonData = NSJSONSerialization.JSONObjectWithData(data,options:NSJSONReadingOptions.MutableContainers,error:nil) as NSDictionary
                
                dispatch_async(dispatch_get_main_queue(),
                    {
                        completionHandle(data:jsonData)
                        
                    })
            }
            })
    }
    
}
